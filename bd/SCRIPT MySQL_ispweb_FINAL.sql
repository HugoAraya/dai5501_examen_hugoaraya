﻿-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-07-2018 a las 23:44:47
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ispweb`
--
CREATE DATABASE IF NOT EXISTS `ispweb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ispweb`;

DELIMITER $$
--
-- Procedimientos
--
DROP PROCEDURE IF EXISTS `GET_CATEGORIA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_CATEGORIA` ()  BEGIN
	SELECT *  FROM categoria_empleado  ;
END$$

DROP PROCEDURE IF EXISTS `GET_FILTRAR_MUESTRAS1`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_FILTRAR_MUESTRAS1` (`CodigoCliente_in` VARCHAR(255))  BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado) 
WHERE  R.codigo_recepcion = CodigoCliente_in 
AND M.estado_id  = 1;
END$$

DROP PROCEDURE IF EXISTS `GET_FILTRAR_MUESTRAS2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_FILTRAR_MUESTRAS2` (`CodigoCliente_in` VARCHAR(255))  BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado) 
WHERE  R.codigo_recepcion = CodigoCliente_in
ORDER BY R.id_recepcion DESC;
END$$

DROP PROCEDURE IF EXISTS `GET_LOGIN_EMPLEADO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_LOGIN_EMPLEADO` (IN `RUT_IN` VARCHAR(255), IN `PASSWORD_IN` VARCHAR(255))  BEGIN
   SELECT E.id_empleado, E.rut_empleado, E.nombre_empleado, E.password_empleado, E.categoria_id ,C.desc_categoria
  FROM empleado E JOIN categoria_empleado C ON (E.categoria_id = C.id_categoria)
  WHERE  rut_empleado = RUT_IN AND password_empleado = PASSWORD_IN ;
END$$

DROP PROCEDURE IF EXISTS `GET_LOGIN_EMPRESA`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_LOGIN_EMPRESA` (`RUT_IN` VARCHAR(255), `PASSWORD_IN` VARCHAR(255))  BEGIN
	SELECT `id_empresa`, `codigo`, `rut_empresa`, `nombre_empresa`, `password_empresa`, `direccion_empresa` 
  FROM `empresa`
  WHERE  rut_empresa = RUT_IN AND password_empresa = PASSWORD_IN ;
END$$

DROP PROCEDURE IF EXISTS `GET_LOGIN_PARTICULAR`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_LOGIN_PARTICULAR` (`RUT_IN` VARCHAR(255), `PASSWORD_IN` VARCHAR(255))  BEGIN
	SELECT `id_particular`, `codigo`, `rut_particular`, `pasword_particular`, `nombre_particular`, `direccion_particular`, `fono_particular`, `email_particular` 
  FROM `particular` 
  WHERE  rut_particular = RUT_IN AND pasword_particular = PASSWORD_IN ;
END$$

DROP PROCEDURE IF EXISTS `GET_MUESTRAS1`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_MUESTRAS1` ()  BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado) 
WHERE M.estado_id = 1
ORDER BY M.id_resultado DESC ;
END$$

DROP PROCEDURE IF EXISTS `GET_MUESTRAS2`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_MUESTRAS2` ()  BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado) 
WHERE M.estado_id = 2
ORDER BY M.id_resultado DESC ;
END$$

DROP PROCEDURE IF EXISTS `GET_RESSULTADO`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_RESSULTADO` (`id_recepcion_in` VARCHAR(255))  BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado,  M.PPM_resultado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado) 
WHERE M.recepcion_id = id_recepcion_in
ORDER BY M.id_resultado DESC ;
END$$

DROP PROCEDURE IF EXISTS `GET_ULTIMO_ID_RECEP`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GET_ULTIMO_ID_RECEP` ()  BEGIN
SELECT MAX(id_recepcion) AS ID FROM recepcion_muestra ;
END$$

--
-- Funciones
--
DROP FUNCTION IF EXISTS `CREATE_CONTACTO`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_CONTACTO` (`id_contacto_in` INTEGER, `rut_contacto_in` VARCHAR(255), `nombre_contacto_in` VARCHAR(255), `email_contacto_in` VARCHAR(255), `fono_contacto_in` VARCHAR(255), `empresa_id_in` INTEGER) RETURNS INT(11) BEGIN
INSERT INTO `contacto_empresa`
(`id_contacto`, `rut_contacto`, `nombre_contacto`, `email_contacto`, `fono_contacto`, `empresa_id`) 
VALUES (id_contacto_in , rut_contacto_in , nombre_contacto_in , email_contacto_in , fono_contacto_in , empresa_id_in );
  RETURN 1;
END$$

DROP FUNCTION IF EXISTS `CREATE_EMPRESA`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_EMPRESA` (`id_empresa_in` INTEGER, `codigo_empresa_in` VARCHAR(255), `rut_empresa_in` VARCHAR(255), `nombre_empresa_in` VARCHAR(255), `password_empresa_in` VARCHAR(255), `direccion_empresa_in` VARCHAR(255)) RETURNS INT(11) BEGIN
INSERT INTO `empresa` (`id_empresa`, `codigo`, `rut_empresa`, `nombre_empresa`, `password_empresa`, `direccion_empresa`) VALUES (id_empresa_in,codigo_empresa_in,rut_empresa_in,nombre_empresa_in,password_empresa_in,direccion_empresa_in);
  RETURN 1;
END$$

DROP FUNCTION IF EXISTS `CREATE_PARTICULAR`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_PARTICULAR` (`id_particular_in` INTEGER, `codigo_particular_in` VARCHAR(255), `rut_particular_in` VARCHAR(255), `pasword_particular_in` VARCHAR(255), `nombre_particular_in` VARCHAR(255), `direccion_particular_in` VARCHAR(255), `fono_particular_in` VARCHAR(255), `email_particular_in` VARCHAR(255)) RETURNS INT(11) BEGIN
  INSERT INTO `particular`(`id_particular`, `codigo`, `rut_particular`, `pasword_particular`, `nombre_particular`, `direccion_particular`, `fono_particular`, `email_particular`)  VALUES ( id_particular_in , codigo_particular_in , rut_particular_in , pasword_particular_in , nombre_particular_in , direccion_particular_in , fono_particular_in , email_particular_in );
  RETURN 1;
END$$

DROP FUNCTION IF EXISTS `CREATE_RECEPCION`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_RECEPCION` (`id_recepcion_in` INTEGER, `fecha_recepcion_in` DATE, `temperatura_recepcion_in` INTEGER, `cantidad_recepcion_in` INTEGER, `codigo_recepcion_in` VARCHAR(255), `rut_empleado_in` VARCHAR(255)) RETURNS INT(11) BEGIN
INSERT INTO recepcion_muestra
( id_recepcion,fecha_recepcion,temperatura_recepcion,cantidad_recepcion,codigo_recepcion,rut_empleado ) 
VALUES (id_recepcion_in , fecha_recepcion_in , temperatura_recepcion_in, cantidad_recepcion_in , codigo_recepcion_in , rut_empleado_in );
  RETURN 1;
END$$

DROP FUNCTION IF EXISTS `CREATE_REGISTRO`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_REGISTRO` (`id_resultado_in` INTEGER, `tipo_id_in` INTEGER, `recepcion_id_in` INTEGER, `fecha_resultado_in` DATE, `PPM_resultado_in` VARCHAR(255), `estado_id_in` INTEGER(255), `rut_empleado_in` VARCHAR(255)) RETURNS INT(11) BEGIN
INSERT INTO resultado_muestra
( id_resultado,tipo_id,recepcion_id,fecha_resultado,PPM_resultado,estado_id,rut_empleado ) 
VALUES (id_resultado_in,tipo_id_in,recepcion_id_in,fecha_resultado_in,PPM_resultado_in,estado_id_in,rut_empleado_in);
  RETURN 1;
END$$

DROP FUNCTION IF EXISTS `GetIDCodigoEmpresa`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `GetIDCodigoEmpresa` (`codigo_IN` VARCHAR(255)) RETURNS INT(11) BEGIN
		RETURN IF ((select count(codigo) from empresa where codigo = codigo_IN ) <> 0, 1, 0);
END$$

DROP FUNCTION IF EXISTS `GetIDCodigoParticular`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `GetIDCodigoParticular` (`codigo_IN` VARCHAR(255)) RETURNS INT(11) BEGIN
		RETURN IF ((select count(codigo) from particular where codigo = codigo_IN ) <> 0, 1, 0);
END$$

DROP FUNCTION IF EXISTS `GetIDEmpresa`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `GetIDEmpresa` (`RUT_IN` VARCHAR(255)) RETURNS INT(11) BEGIN
		RETURN IF ((select count(id_empresa) from empresa where rut_empresa = RUT_IN ) <> 0, 1, 0);
END$$

DROP FUNCTION IF EXISTS `GetIDParticular`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `GetIDParticular` (`RUT_IN` VARCHAR(255)) RETURNS INT(11) BEGIN
	RETURN IF ((select count(id_particular) from particular where rut_particular = RUT_IN ) <> 0, 1, 0);
END$$

DROP FUNCTION IF EXISTS `GET_ID_EMPRESA_X_RUT`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `GET_ID_EMPRESA_X_RUT` (`RUT_IN` VARCHAR(255)) RETURNS INT(11) BEGIN
	RETURN ( SELECT id_empresa FROM empresa WHERE rut_empresa = RUT_IN);
END$$

DROP FUNCTION IF EXISTS `UPDATE_REGISTRO`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `UPDATE_REGISTRO` (`id_resultado_in` INT(11), `fechaResult_in` DATE, `PPM_resultado_in` INT(11), `estado_id_in` INT(11), `rut_empleado_in` VARCHAR(255)) RETURNS INT(11) BEGIN
 UPDATE resultado_muestra
 SET fecha_resultado = fechaResult_in,
   PPM_resultado = PPM_resultado_in,
    estado_id = estado_id_in,
    rut_empleado = rut_empleado_in
 WHERE id_resultado = id_resultado_in;
	RETURN 1;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria_empleado`
--

DROP TABLE IF EXISTS `categoria_empleado`;
CREATE TABLE `categoria_empleado` (
  `id_categoria` int(11) NOT NULL,
  `desc_categoria` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categoria_empleado`
--

INSERT INTO `categoria_empleado` (`id_categoria`, `desc_categoria`) VALUES
(1, 'Administrador'),
(2, 'Recepcionista'),
(3, 'Tecnico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto_empresa`
--

DROP TABLE IF EXISTS `contacto_empresa`;
CREATE TABLE `contacto_empresa` (
  `id_contacto` int(11) NOT NULL,
  `rut_contacto` varchar(255) DEFAULT NULL,
  `nombre_contacto` varchar(255) DEFAULT NULL,
  `email_contacto` varchar(255) DEFAULT NULL,
  `fono_contacto` varchar(255) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `contacto_empresa`
--

INSERT INTO `contacto_empresa` (`id_contacto`, `rut_contacto`, `nombre_contacto`, `email_contacto`, `fono_contacto`, `empresa_id`) VALUES
(1, '2222222', 'ss222', 's@asas22', '222222', 11),
(2, '2222222', 'ss', 's@asas.cl', '222222', 12),
(3, '2222222', 'Contacto 11', 's@asas.cl22211', '222222', 13),
(4, '2222222', 'Contacto 1', 's@asas.cl', '222222', 14),
(5, '2222', '1234', 's@asas.cl', '11221', 15),
(6, '2222222111', 'Contacto 1', 's@asas', '123125', 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

DROP TABLE IF EXISTS `empleado`;
CREATE TABLE `empleado` (
  `id_empleado` int(11) NOT NULL,
  `rut_empleado` varchar(255) DEFAULT NULL,
  `nombre_empleado` varchar(255) DEFAULT NULL,
  `password_empleado` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id_empleado`, `rut_empleado`, `nombre_empleado`, `password_empleado`, `categoria_id`) VALUES
(1, '1111', 'admin', '1234', 1),
(2, '2222', 'recp', '1234', 2),
(3, '3333', 'tec', '1234', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL,
  `codigo` varchar(255) DEFAULT NULL,
  `rut_empresa` varchar(255) DEFAULT NULL,
  `nombre_empresa` varchar(255) DEFAULT NULL,
  `password_empresa` varchar(255) DEFAULT NULL,
  `direccion_empresa` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id_empresa`, `codigo`, `rut_empresa`, `nombre_empresa`, `password_empresa`, `direccion_empresa`) VALUES
(1, 'EMPRE112', '1111111', 'Empresa 1', '1234', 'av siempre viva 666'),
(2, 'EMPRE122', '11111112', '11111112', '1234', 'av siempre viva 666'),
(3, 'EMPRE223', '1111111222', 'Empresa 2', '1234', 'av siempre viva 666'),
(4, 'EMPRE132', '1111111222333', 'Empresa 2', '1234', 'av siempre viva 666'),
(5, 'EMPRE112', '111111122233344', 'Empresa 2', '1234', 'av siempre viva 666'),
(6, 'EMPRE155', '111111188', 'Empresa 3', '1234', 'av siempre viva 666'),
(7, 'EMPRE151', '111111199', 'Empresa 3', '1234', 'av siempre viva 666'),
(8, 'EMPRE122', '1122222', 'Empresa 3', '1234', 'av siempre viva 666'),
(9, 'EMPRE235', '1235676543', 'Empresa 8', '1234', 'av siempre viva 666'),
(10, 'EMPRE222', '222222211112', 'Empresa 8', '1234', 'av siempre viva 666'),
(11, 'EMPRE334', '33344444', 'Empresa 8', '1234', 'av siempre viva 666'),
(12, 'EMPRE000', '10001000', 'Empresa 1000', '1234', 'av siempre viva 666'),
(13, 'EMPRE535', '153534780', 'Empresa 11', '1234', 'av siempre viva 666'),
(14, 'EMPRE155', '1111111423423', '1111111', '1234', 'av siempre viva 666'),
(15, 'EMPRE111', '1111', 'IthinkChile', '1234', 'av siempre viva 666'),
(17, 'EMPRE232', '12324444', '12321456', '1234', 'av siempre viva 666');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_resultado`
--

DROP TABLE IF EXISTS `estado_resultado`;
CREATE TABLE `estado_resultado` (
  `id_estado` int(11) NOT NULL,
  `desc_estado` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `estado_resultado`
--

INSERT INTO `estado_resultado` (`id_estado`, `desc_estado`) VALUES
(1, 'Pendiente'),
(2, 'Procesado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `particular`
--

DROP TABLE IF EXISTS `particular`;
CREATE TABLE `particular` (
  `id_particular` int(11) NOT NULL,
  `codigo` varchar(255) DEFAULT NULL,
  `rut_particular` varchar(255) DEFAULT NULL,
  `pasword_particular` varchar(255) DEFAULT NULL,
  `nombre_particular` varchar(255) DEFAULT NULL,
  `direccion_particular` varchar(255) DEFAULT NULL,
  `fono_particular` varchar(255) DEFAULT NULL,
  `email_particular` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `particular`
--

INSERT INTO `particular` (`id_particular`, `codigo`, `rut_particular`, `pasword_particular`, `nombre_particular`, `direccion_particular`, `fono_particular`, `email_particular`) VALUES
(3, 'PERSO111', '1111', '1234', 'hugo', 'campos de hielo 1584 maipu', '98601037', 'sdad@sad.cl'),
(4, 'perso198', '6198124-1', '1234', '1234', '1234', '98601037', 'sdad@sad.cl'),
(5, 'perso198', '6198124-1', '1234', '1234', '1234', '98601037', 'sdad@sad.cll'),
(6, 'perso198', '6198124-1', '1234', '122121sdsd', 'aa', '98601037', 'sdad@sad.cl'),
(7, 'perso112', '1112222', '123', 'hugo', 'aa', '98601037', 'hugobg@hotmail.com'),
(8, 'perso112', '1112222', '123', 'hugo', 'aa', '98601037', 'hugobg@hotmail.com'),
(9, 'perso112', '1112222', '1234', 'hugos', 'aaaaalejos', '212112', 'sdad@sad.cl'),
(10, 'perso112', '1112222', '1234', 'hugos', 'aaaaalejos', '212112', 'sdad@sad.cl'),
(12, 'perso112', '1112222', '1234', 'hugos', 'aaaaalejos', '212112', 'sdad@sad.cl'),
(13, 'perso11', '612224-12', '123', '122121sdsd', 'sddssd', '98601037', '12312@asdads'),
(14, 'perso111', '11111111', '1111', '1111', '111111', '123132', '122121@asdas.cl'),
(15, 'persoqwe', 'eqwe', 'qweqwe', 'qweqw', 'qweqwe', 'qweqwe', 'asddas@asdas'),
(16, 'persowwe', 'qwweq', 'qweqw', 'qweqwe', 'qweqwe', '123132', 'qweqwe@sad.cl'),
(17, 'perso198', '6198124-1', '1234', 'hugo', 'asdasddas', '98601037', '12312@asdads'),
(18, 'perso21', '121', 'wqe', '12312', '123123', '986010377', '12312@asdads'),
(19, 'perso111', '11118888', '1234', '122121sdsd', 'aa', '98601037', 'hugobg@hotmail.com'),
(20, 'perso111', '111188888', '1321', '122112', '211212', '122', '213123213@assad'),
(21, 'perso198', '6198124-12', '123', 'hugo', 'sddssd', '98601037', 'hugobg@hotmail.com'),
(22, 'PERSO231', '12312312', '12312', '122121sdsd', 'sddssd', '123132', 'hugobg@hotmail.com'),
(23, 'PERSO231', '123123123', '123', '1212', 'aa', '98601037', '12312@asdads'),
(24, 'PERSO198', '6198124-122', '1234', '122121sdsd', 'aa', '98601037', '12312@asdads'),
(26, 'PERSO231', '12314', '1234', '122121sdsd', 'asdasddas', '12345', 'hugobg@hotmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recepcion_muestra`
--

DROP TABLE IF EXISTS `recepcion_muestra`;
CREATE TABLE `recepcion_muestra` (
  `id_recepcion` int(11) NOT NULL,
  `fecha_recepcion` date DEFAULT NULL,
  `temperatura_recepcion` int(11) DEFAULT NULL,
  `cantidad_recepcion` int(11) DEFAULT NULL,
  `codigo_recepcion` varchar(255) DEFAULT NULL,
  `rut_empleado` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `recepcion_muestra`
--

INSERT INTO `recepcion_muestra` (`id_recepcion`, `fecha_recepcion`, `temperatura_recepcion`, `cantidad_recepcion`, `codigo_recepcion`, `rut_empleado`) VALUES
(1, '2018-07-27', 1221, 1221, 'EMPRE122', '1111'),
(2, '2018-07-24', 2, 2, 'asdas', '1111'),
(3, '2018-07-26', 123, 2321, '12312', '1111'),
(4, '2018-07-13', 1, 1, 'EMPRE111', '1111'),
(5, '2018-07-20', 4, 6, 'perso198', '1111'),
(6, '2018-07-19', 4, 5, 'perso198', '1111'),
(7, '2018-07-19', 4, 5, 'perso198', '1111'),
(8, '2018-07-10', 111, 111, '1221', '1111'),
(9, '2018-07-20', 12, 12, 'perso198', '1111'),
(10, '2018-07-19', 4, 6, 'perso198', '1111'),
(11, '2018-07-19', 1212, 1212, 'perso198', '1111'),
(12, '2018-07-19', 1212, 1212, 'perso198', '1111'),
(13, '2018-07-13', 13, 12312, 'perso198', '1111'),
(14, '2018-07-27', 1212, 1212, 'perso198', '1111'),
(15, '2018-07-13', 1212, 1212, 'perso198', '1111'),
(16, '2018-07-13', 11, 11, 'perso198', '1111'),
(17, '2018-07-15', 1, 1, 'perso198', '1111'),
(18, '2018-07-28', 12122, 1212, 'perso198', '1111'),
(19, '2018-07-19', 2, 3, 'perso198', '1111'),
(20, '2018-07-19', 2, 3, 'perso198', '1111'),
(21, '2018-07-19', 2, 3, 'perso198', '1111'),
(22, '2018-07-19', 3, 4, 'perso198', '1111'),
(23, '2018-07-13', 1221, 1212, 'perso198', '1111'),
(24, '2018-07-20', 1212, 1221, 'perso198', '1111'),
(25, '2018-07-20', 2, 1, 'perso198', '1111'),
(26, '2018-07-19', 21, 12, 'perso198', '1111'),
(27, '2018-07-12', 21, 12, 'perso198', '1111'),
(28, '2018-07-12', 21, 12, 'perso198', '1111'),
(29, '2018-07-06', 12112, 1221, 'perso198', '1111'),
(30, '2018-07-06', 12112, 1221, 'perso198', '1111'),
(31, '2018-07-06', 12112, 1221, 'perso198', '1111'),
(32, '2018-07-06', 12112, 1221, 'perso198', '1111'),
(33, '2018-07-20', 1221, 2112, 'perso198', '1111'),
(34, '2018-07-20', 13, 21, 'perso198', '1111'),
(35, '2018-07-19', 2121, 1221, 'perso198', '1111'),
(36, '2018-07-19', 1221, 1212, 'EMPRE111', '1111'),
(37, '2018-07-13', 3, 4, 'EMPRE111', '1111'),
(38, '2018-07-12', 2222, 1111, 'EMPRE111', '2222'),
(39, '2018-07-19', 121, 1212, 'perso198', '1111'),
(40, '2018-07-20', 122121, 12122, 'perso198', '1111'),
(41, '2018-07-26', 2, 3, 'EMPRE111', '1111'),
(42, '2018-07-18', 2, 14, 'PERSO111', '2222'),
(43, '2018-06-29', 1212, 1212, 'EMPRE111', '1111'),
(44, '2018-07-13', 122121, 3333, 'EMPRE111', '1111'),
(45, '2018-07-20', 111, 11, 'perso198', '1111'),
(46, '0000-00-00', 1234, 12314, 'perso198', '1111'),
(47, '2018-07-18', 3, 3, '1221', '1111'),
(48, '2018-07-19', 3, 2, '1231412412341243245', '1111'),
(49, '2018-07-26', 1221, 1212, 'perso198', '1111'),
(50, '2018-07-06', 112, 1212, '1231234', '1111'),
(51, '2018-07-12', 1223, 12321, '09089769868754', '1111'),
(52, '2018-07-21', 123123, 123123, '1231234124', '1111'),
(53, '2018-07-11', 1, 12121, 'perso198', '1111'),
(54, '2018-07-26', 122112, 121221, 'EMPRE223', '1111'),
(55, '2018-07-25', 122112, 121212, 'adssd213213', '1111'),
(56, '2018-07-18', 121, 1212, '123123', '1111'),
(57, '2018-07-25', 123, 12312, '213123123', '1111'),
(58, '2018-07-26', 1221, 1221, 'perso198', '1111'),
(59, '2018-07-12', 1221, 1212, 'EMPRE111', '1111'),
(60, '2018-07-19', 121212, 122112, 'perso198', '1111'),
(61, '2018-07-13', 122112, 121212, 'EMPRE111', '1111'),
(62, '2018-07-12', 1221, 12122, 'EMPRE111', '1111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resultado_muestra`
--

DROP TABLE IF EXISTS `resultado_muestra`;
CREATE TABLE `resultado_muestra` (
  `id_resultado` int(11) NOT NULL,
  `tipo_id` int(11) DEFAULT NULL,
  `recepcion_id` int(11) DEFAULT NULL,
  `fecha_resultado` date DEFAULT NULL,
  `PPM_resultado` int(11) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `rut_empleado` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `resultado_muestra`
--

INSERT INTO `resultado_muestra` (`id_resultado`, `tipo_id`, `recepcion_id`, `fecha_resultado`, `PPM_resultado`, `estado_id`, `rut_empleado`) VALUES
(14, 1, 35, NULL, NULL, 1, NULL),
(15, 5, 35, '2018-07-11', 2, 2, '1111'),
(16, 1, 36, NULL, NULL, 1, NULL),
(17, 2, 36, NULL, NULL, 1, NULL),
(18, 1, 37, NULL, NULL, 1, NULL),
(19, 2, 37, NULL, NULL, 1, NULL),
(20, 1, 38, '2018-07-19', 124, 2, '1111'),
(21, 2, 38, '2018-07-12', 2, 2, '1111'),
(22, 3, 38, NULL, NULL, 1, NULL),
(23, 1, 39, NULL, NULL, 1, NULL),
(24, 3, 39, '2018-07-14', 2222, 2, '1111'),
(25, 4, 39, '2018-07-13', 1234, 2, '1111'),
(26, 1, 40, NULL, NULL, 1, NULL),
(27, 2, 40, NULL, NULL, 1, NULL),
(28, 3, 40, '2018-07-14', 211212, 2, '1111'),
(29, 1, 41, '2018-07-19', 1212, 2, '1111'),
(30, 2, 41, '2018-07-19', 1222, 2, '1111'),
(31, 1, 42, '2018-07-21', 1500, 2, '3333'),
(32, 2, 42, '2018-07-13', 1123, 2, '3333'),
(33, 3, 42, '2018-07-18', 1234, 2, '3333'),
(34, 4, 42, NULL, NULL, 1, NULL),
(35, 5, 42, NULL, NULL, 1, NULL),
(36, 1, 43, NULL, NULL, 1, NULL),
(37, 2, 43, NULL, NULL, 1, NULL),
(38, 3, 43, NULL, NULL, 1, NULL),
(39, 4, 43, NULL, NULL, 1, NULL),
(40, 5, 43, NULL, NULL, 1, NULL),
(41, 1, 44, NULL, NULL, 1, NULL),
(42, 3, 44, NULL, NULL, 1, NULL),
(43, 5, 44, NULL, NULL, 1, NULL),
(44, 2, 45, NULL, NULL, 1, NULL),
(45, 3, 45, NULL, NULL, 1, NULL),
(46, 4, 45, '2018-07-26', 2222, 2, '1111'),
(47, 2, 46, NULL, NULL, 1, NULL),
(48, 4, 46, '2018-07-18', 1234, 2, '1111'),
(49, 3, 47, NULL, NULL, 1, NULL),
(50, 3, 48, NULL, NULL, 1, NULL),
(51, 1, 49, NULL, NULL, 1, NULL),
(52, 1, 50, NULL, NULL, 1, NULL),
(53, 2, 50, NULL, NULL, 1, NULL),
(54, 3, 50, NULL, NULL, 1, NULL),
(55, 1, 51, NULL, NULL, 1, NULL),
(56, 2, 51, NULL, NULL, 1, NULL),
(57, 2, 52, NULL, NULL, 1, NULL),
(58, 3, 52, NULL, NULL, 1, NULL),
(59, 2, 53, NULL, NULL, 1, NULL),
(60, 3, 53, NULL, NULL, 1, NULL),
(61, 1, 54, NULL, NULL, 1, NULL),
(62, 2, 54, NULL, NULL, 1, NULL),
(63, 4, 55, NULL, NULL, 1, NULL),
(64, 5, 55, NULL, NULL, 1, NULL),
(65, 3, 56, NULL, NULL, 1, NULL),
(66, 4, 56, NULL, NULL, 1, NULL),
(67, 1, 57, NULL, NULL, 1, NULL),
(68, 2, 57, NULL, NULL, 1, NULL),
(69, 1, 58, NULL, NULL, 1, NULL),
(70, 2, 58, NULL, NULL, 1, NULL),
(71, 1, 59, NULL, NULL, 1, NULL),
(72, 2, 59, NULL, NULL, 1, NULL),
(73, 1, 60, NULL, NULL, 1, NULL),
(74, 2, 60, '2018-07-28', 123123, 2, '1111'),
(75, 2, 61, NULL, NULL, 1, NULL),
(76, 3, 61, NULL, NULL, 1, NULL),
(77, 1, 62, NULL, NULL, 1, NULL),
(78, 2, 62, NULL, NULL, 1, NULL),
(79, 3, 62, '2018-07-05', 122112, 2, '1111');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_analisis`
--

DROP TABLE IF EXISTS `tipo_analisis`;
CREATE TABLE `tipo_analisis` (
  `id_tipo` int(11) NOT NULL,
  `det_tipoanalisis` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tipo_analisis`
--

INSERT INTO `tipo_analisis` (`id_tipo`, `det_tipoanalisis`) VALUES
(1, 'Micotoxinas'),
(2, 'Metales pesados'),
(3, 'Plaguicidas prohibidos'),
(4, 'Marea roja '),
(5, 'Bacterias nocivas');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria_empleado`
--
ALTER TABLE `categoria_empleado`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `contacto_empresa`
--
ALTER TABLE `contacto_empresa`
  ADD PRIMARY KEY (`id_contacto`),
  ADD KEY `contacto_empresa` (`empresa_id`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id_empleado`),
  ADD KEY `empleado_categoria` (`categoria_id`);

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id_empresa`);

--
-- Indices de la tabla `estado_resultado`
--
ALTER TABLE `estado_resultado`
  ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `particular`
--
ALTER TABLE `particular`
  ADD PRIMARY KEY (`id_particular`);

--
-- Indices de la tabla `recepcion_muestra`
--
ALTER TABLE `recepcion_muestra`
  ADD PRIMARY KEY (`id_recepcion`);

--
-- Indices de la tabla `resultado_muestra`
--
ALTER TABLE `resultado_muestra`
  ADD PRIMARY KEY (`id_resultado`),
  ADD KEY `resultado_recep` (`recepcion_id`),
  ADD KEY `resultado_estado` (`estado_id`),
  ADD KEY `resultado_tipo` (`tipo_id`);

--
-- Indices de la tabla `tipo_analisis`
--
ALTER TABLE `tipo_analisis`
  ADD PRIMARY KEY (`id_tipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `contacto_empresa`
--
ALTER TABLE `contacto_empresa`
  MODIFY `id_contacto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id_empleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id_empresa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `particular`
--
ALTER TABLE `particular`
  MODIFY `id_particular` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `recepcion_muestra`
--
ALTER TABLE `recepcion_muestra`
  MODIFY `id_recepcion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT de la tabla `resultado_muestra`
--
ALTER TABLE `resultado_muestra`
  MODIFY `id_resultado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `contacto_empresa`
--
ALTER TABLE `contacto_empresa`
  ADD CONSTRAINT `contacto_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id_empresa`);

--
-- Filtros para la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD CONSTRAINT `empleado_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria_empleado` (`id_categoria`);

--
-- Filtros para la tabla `resultado_muestra`
--
ALTER TABLE `resultado_muestra`
  ADD CONSTRAINT `resultado_estado` FOREIGN KEY (`estado_id`) REFERENCES `estado_resultado` (`id_estado`),
  ADD CONSTRAINT `resultado_recep` FOREIGN KEY (`recepcion_id`) REFERENCES `recepcion_muestra` (`id_recepcion`),
  ADD CONSTRAINT `resultado_tipo` FOREIGN KEY (`tipo_id`) REFERENCES `tipo_analisis` (`id_tipo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
