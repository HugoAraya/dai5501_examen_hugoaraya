﻿
DROP DATABASE IF EXISTS ispweb ;
CREATE DATABASE ispweb ;
USE ispweb ;

#
# Table structure for table categoria_empleado
#

CREATE TABLE `categoria_empleado` (
  `id_categoria` int(11) NOT NULL,
  `desc_categoria` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `categoria_empleado` VALUES (1,'Administrador');
INSERT INTO `categoria_empleado` VALUES (2,'Recepcionista');
INSERT INTO `categoria_empleado` VALUES (3,'Tecnico');

#
# Table structure for table contacto_empresa
#

CREATE TABLE `contacto_empresa` (
  `id_contacto` int(11) NOT NULL AUTO_INCREMENT,
  `rut_contacto` varchar(255) DEFAULT NULL,
  `nombre_contacto` varchar(255) DEFAULT NULL,
  `email_contacto` varchar(255) DEFAULT NULL,
  `fono_contacto` varchar(255) DEFAULT NULL,
  `empresa_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_contacto`),
  KEY `contacto_empresa` (`empresa_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Table structure for table empleado
#

CREATE TABLE `empleado` (
  `id_empleado` int(11) NOT NULL AUTO_INCREMENT,
  `rut_empleado` varchar(255) DEFAULT NULL,
  `nombre_empleado` varchar(255) DEFAULT NULL,
  `password_empleado` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_empleado`),
  KEY `empleado_categoria` (`categoria_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Table structure for table empresa
#

CREATE TABLE `empresa` (
  `id_empresa` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_empresa` varchar(255) DEFAULT NULL,
  `rut_empresa` varchar(255) DEFAULT NULL,
  `nombre_empresa` varchar(255) DEFAULT NULL,
  `password_empresa` varchar(255) DEFAULT NULL,
  `direccion_empresa` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_empresa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Table structure for table estado_resultado
#

CREATE TABLE `estado_resultado` (
  `id_estado` int(11) NOT NULL,
  `desc_estado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_estado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `estado_resultado` VALUES (1,NULL);

#
# Table structure for table particular
#

CREATE TABLE `particular` (
  `id_particular` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_particular` varchar(255) DEFAULT NULL,
  `rut_particular` varchar(255) DEFAULT NULL,
  `pasword_particular` varchar(255) DEFAULT NULL,
  `nombre_particular` varchar(255) DEFAULT NULL,
  `direccion_particular` varchar(255) DEFAULT NULL,
  `fono_particular` varchar(255) DEFAULT NULL,
  `email_particular` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_particular`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



#
# Table structure for table tipo_analisis
#

CREATE TABLE `tipo_analisis` (
  `id_tipo` int(11) DEFAULT NULL,    
  `det_tipoanalisis` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
INSERT INTO `tipo_analisis` VALUES (1,'Micotoxinas');
INSERT INTO `tipo_analisis` VALUES (2,'Metales pesados');
INSERT INTO `tipo_analisis` VALUES (3,'Plaguicidas prohibidos');
INSERT INTO `tipo_analisis` VALUES (4,'Marea roja ');
INSERT INTO `tipo_analisis` VALUES (5,'Bacterias nocivas');

#



#
# Table structure for table recepcion_muestra
#

CREATE TABLE `recepcion_muestra` (
  `id_recepcion` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_recepcion` date DEFAULT NULL,
  `temperatura_recepcion` int(11) DEFAULT NULL,
  `cantidad_recepcion` int(11) DEFAULT NULL,
  `codigo_recepcion` varchar(255) DEFAULT NULL,
  `rut_empleado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_recepcion`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

#
# Table structure for table resultado_muestra
#

CREATE TABLE `resultado_muestra` (
  `id_resultado` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_id` int(11) DEFAULT NULL,
  `recepcion_id` int(11) DEFAULT NULL,
  `fecha_resultado` date DEFAULT NULL,
  `PPM_resultado` int(11) DEFAULT NULL,
  `estado_id` int(11) DEFAULT NULL,
  `rut_empleado` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_resultado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;




#  Foreign keys for table contacto_empresa
#

ALTER TABLE `contacto_empresa`
ADD CONSTRAINT `contacto_empresa` FOREIGN KEY (`empresa_id`) REFERENCES `empresa` (`id_empresa`);

#
#  Foreign keys for table empleado
#

ALTER TABLE `empleado`
ADD CONSTRAINT `empleado_categoria` FOREIGN KEY (`categoria_id`) REFERENCES `categoria_empleado` (`id_categoria`);



#
#  Foreign keys for table resultado_muestra
#

ALTER TABLE `resultado_muestra`
ADD CONSTRAINT `resultado_recep` FOREIGN KEY (`recepcion_id`) REFERENCES `recepcion_muestra` (`id_recepcion`);



ALTER TABLE `resultado_muestra`
ADD CONSTRAINT `resultado_estado` FOREIGN KEY (`estado_id`) REFERENCES `estado_resultado` (`id_estado`);


ALTER TABLE `resultado_muestra`
ADD CONSTRAINT `resultado_tipo` FOREIGN KEY (`tipo_id`) REFERENCES `tipo_analisis` (`id_tipo`);


DELIMITER //
CREATE PROCEDURE GET_CATEGORIA()
BEGIN
	SELECT *  FROM categoria_empleado  ;
END //


DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_PARTICULAR`(
  id_particular_in INTEGER,
  codigo_particular_in VARCHAR(255),
  rut_particular_in VARCHAR(255),
  pasword_particular_in  VARCHAR(255),
  nombre_particular_in VARCHAR(255),
  direccion_particular_in VARCHAR(255),
  fono_particular_in VARCHAR(255),
  email_particular_in VARCHAR(255)) RETURNS int(11)
    DETERMINISTIC
BEGIN
  INSERT INTO `particular`(`id_particular`, `codigo_particular`, `rut_particular`, `pasword_particular`, `nombre_particular`, `direccion_particular`, `fono_particular`, `email_particular`)  VALUES ( id_particular_in , codigo_particular_in , rut_particular_in , pasword_particular_in , nombre_particular_in , direccion_particular_in , fono_particular_in , email_particular_in );
  RETURN 1;
END  //

DELIMITER //
CREATE OR REPLACE FUNCTION GetIDParticular(RUT_IN VARCHAR(255))
RETURNS INTEGER  DETERMINISTIC
BEGIN
		RETURN IF ((select count(id_particular) from particular where rut_particular = RUT_IN ) <> 0, 1, 0);
END //

DELIMITER //
CREATE OR REPLACE FUNCTION GetIDEmpresa(RUT_IN VARCHAR(255))
RETURNS INTEGER  DETERMINISTIC
BEGIN
		RETURN IF ((select count(id_empresa) from empresa where rut_empresa = RUT_IN ) <> 0, 1, 0);
END //




DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_EMPRESA`(
  id_empresa_in INTEGER,
  codigo_empresa_in VARCHAR(255),
  rut_empresa_in VARCHAR(255),
  nombre_empresa_in  VARCHAR(255),
  password_empresa_in VARCHAR(255),
  direccion_empresa_in VARCHAR(255))
  RETURNS int(11)
  DETERMINISTIC
BEGIN
INSERT INTO `empresa`
(`id_empresa`, `codigo_empresa`, `rut_empresa`, `nombre_empresa`, `password_empresa`, `direccion_empresa`) 
VALUES (id_empresa_in,codigo_empresa_in,rut_empresa_in,nombre_empresa_in,password_empresa_in,direccion_empresa_in)
  RETURN 1;
END  //



DELIMITER //
CREATE FUNCTION GET_ID_EMPRESA_X_RUT( RUT_IN VARCHAR(255) )
RETURNS INTEGER DETERMINISTIC
BEGIN
	RETURN ( SELECT id_empresa FROM empresa WHERE rut_empresa = RUT_IN);
END //


CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_CONTACTO`(
  id_contacto_in INTEGER,
  rut_contacto_in VARCHAR(255),
  nombre_contacto_in VARCHAR(255),
  email_contacto_in  VARCHAR(255),
  fono_contacto_in VARCHAR(255),
  empresa_id INTEGER)
  RETURNS int(11)
  DETERMINISTIC
BEGIN
INSERT INTO `contacto_empresa`
(`id_contacto`, `rut_contacto`, `nombre_contacto`, `email_contacto`, `fono_contacto`, `empresa_id`) 
VALUES (id_contacto_in , rut_contacto_in , nombre_contacto_in , email_contacto_in , fono_contacto_in , empresa_id_in )
  RETURN 1;
END  //



DELIMITER //
CREATE PROCEDURE GET_LOGIN_PARTICULAR( RUT_IN VARCHAR(255), PASSWORD_IN VARCHAR(255) )
BEGIN
	SELECT `id_particular`, `codigo_particular`, `rut_particular`, `pasword_particular`, `nombre_particular`, `direccion_particular`, `fono_particular`, `email_particular` 
  FROM `particular` 
  WHERE  rut_particular = RUT_IN AND pasword_particular = PASSWORD_IN ;
END //



DELIMITER //
CREATE PROCEDURE GET_LOGIN_EMPRESA( RUT_IN VARCHAR(255), PASSWORD_IN VARCHAR(255) )
BEGIN
	SELECT `id_empresa`, `codigo_empresa`, `rut_empresa`, `nombre_empresa`, `password_empresa`, `direccion_empresa` 
  FROM `empresa`
  WHERE  rut_empresa = RUT_IN AND password_empresa = PASSWORD_IN ;
END //


DELIMITER //
CREATE PROCEDURE GET_LOGIN_EMPLEADO( RUT_IN VARCHAR(255), PASSWORD_IN VARCHAR(255) )
BEGIN
  SELECT E.id_empleado, E.rut_empleado, E.nombre_empleado, E.password_empleado, E.categoria_id ,C.desc_categoria
  FROM empleado E JOIN categoria_empleado C ON (E.categoria_id = C.id_categoria)
  WHERE  rut_empleado = RUT_IN AND password_empleado = PASSWORD_IN ;
END //


DELIMITER //
CREATE OR REPLACE FUNCTION GetIDCodigoParticular(codigo_IN VARCHAR(255))
RETURNS INTEGER  DETERMINISTIC
BEGIN
		RETURN IF ((select count(codigo_particular) from particular where codigo_particular = codigo_IN ) <> 0, 1, 0);
END //


DELIMITER //
CREATE OR REPLACE FUNCTION GetIDCodigoEmpresa(codigo_IN VARCHAR(255))
RETURNS INTEGER  DETERMINISTIC
BEGIN
		RETURN IF ((select count(codigo_empresa) from empresa where codigo_empresa = codigo_IN ) <> 0, 1, 0);
END //


DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_RECEPCION`(
  id_recepcion_in INTEGER,
  fecha_recepcion_in DATE,
  temperatura_recepcion_in INTEGER,
  cantidad_recepcion_in  INTEGER,
  codigo_recepcion_in VARCHAR(255),
  rut_empleado_in VARCHAR(255))
  RETURNS int(11)  DETERMINISTIC
BEGIN
INSERT INTO recepcion_muestra
( id_recepcion,fecha_recepcion,temperatura_recepcion,cantidad_recepcion,codigo_recepcion,rut_empleado ) 
VALUES (id_recepcion_in , fecha_recepcion_in , temperatura_recepcion_in, cantidad_recepcion_in , codigo_recepcion_in , rut_empleado_in );
  RETURN 1;
END //


DELIMITER //
CREATE PROCEDURE GET_ULTIMO_ID_RECEP()
BEGIN
SELECT MAX(id_recepcion) FROM recepcion_muestra ;
END //


DELIMITER //
CREATE DEFINER=`root`@`localhost` FUNCTION `CREATE_REGISTRO`(
  id_resultado_in INTEGER,
  tipo_id_in INTEGER,
  recepcion_id_in INTEGER,
  fecha_resultado_in  DATE,
  PPM_resultado_in VARCHAR(255),
  estado_id_in INTEGER(255),
  rut_empleado_in VARCHAR(255))
  RETURNS int(11)  DETERMINISTIC
BEGIN
INSERT INTO resultado_muestra
( id_resultado,tipo_id,recepcion_id,fecha_resultado,PPM_resultado,estado_id,rut_empleado ) 
VALUES (id_resultado_in,tipo_id_in,recepcion_id_in,fecha_resultado_in,PPM_resultado_in,estado_id_in,rut_empleado_in);
  RETURN 1;
END //





DELIMITER //
CREATE PROCEDURE GET_MUESTRAS1()
BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado)  ;
END //

GET_FILTRAR_MUESTRAS1

DELIMITER //
CREATE PROCEDURE GET_FILTRAR_MUESTRAS1(CodigoCliente_in VARCHAR(255))
BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado) 
WHERE  R.codigo_recepcion = CodigoCliente_in ;
END //





DELIMITER //
CREATE OR REPLACE FUNCTION UPDATE_REGISTRO
    (
    id_resultado_in int(11),
    fechaResult_in DATE,
    PPM_resultado_in int(11),
    estado_id_in int(11),
    rut_empleado_in VARCHAR(255)
    )
	RETURNS int(11) DETERMINISTIC
BEGIN
 UPDATE resultado_muestra
 SET fecha_resultado = fechaResult_in,
   PPM_resultado = PPM_resultado_in,
    estado_id = estado_id_in,
    rut_empleado = rut_empleado_in
 WHERE id_resultado = id_resultado_in;
	RETURN 1;
END //

DELIMITER //
CREATE PROCEDURE GET_RESSULTADO(id_recepcion_in VARCHAR(255))
BEGIN
SELECT M.id_resultado, M.tipo_id, T.det_tipoanalisis, E.desc_estado,  M.PPM_resultado, R.codigo_recepcion, R.id_recepcion
FROM resultado_muestra M 
JOIN recepcion_muestra R ON (M.recepcion_id = R.id_recepcion) 
JOIN tipo_Analisis T ON (M.tipo_id = T.id_tipo)
JOIN estado_resultado E ON (M.estado_id = E.id_estado) 
WHERE M.recepcion_id = id_recepcion_in
ORDER BY M.id_resultado DESC ;
END //


