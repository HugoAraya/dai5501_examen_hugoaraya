'use strict';

/**
 * @ngdoc overview
 * @name ispwebApp
 * @description
 * # ispwebApp
 *
 * Main module of the application.
 */
angular
.module('ispwebApp', ['ui.router', 'chart.js', 'LocalStorageModule'  ] )
.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider ) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'views/main.html',
      controller: 'MainCtrl'
    })
    .state('registroCli', {
      url: '/registoCliente',
      templateUrl: 'views/registroCliente.html',
      controller: 'RegistroclienteCtrl'
    })
    .state('intranet', {
      url: '/intranet',
      templateUrl: 'views/intranet.html',
      controller: 'IntranetCtrl'
    })
    .state('mainCliente', {
      url: '/mainCliente',
      templateUrl: 'views/maincliente.html',
      controller: 'MainclienteCtrl'
    })
    .state('mainEmpresa', {
      url: '/mainEmpresa',
      templateUrl: 'views/mainempresa.html',
      controller: 'MainempresaCtrl'
    })
    .state('mainAdministrador', {
      url: '/mainAdministrador',
      templateUrl: 'views/mainadministrador.html',
      controller: 'MainadministradorCtrl'
    })
    .state('mainReceptor', {
      url: '/mainReceptor',
      templateUrl: 'views/mainreceptor.html',
      controller: 'MainreceptorCtrl'
    })
    .state('mainTecnico', {
      url: '/mainTecnico',
      templateUrl: 'views/maintecnico.html',
      controller: 'MaintecnicoCtrl'
    })
    .state('datosCliente', {
      url: '/datosCliente',
      templateUrl: 'views/datoscliente.html',
      controller: 'DatosclienteCtrl'
    })
    .state('datosEmpresa', {
      url: '/datosEmpresa',
      templateUrl: 'views/datosEmpresa.html',
      controller: 'DatosempresaCtrl'
    })
    .state('resultados', {
      url: '/resultados',
      templateUrl: 'views/resultados.html',
      controller: 'ResultadosCtrl'
    })
    .state('busquedaMuestras', {
      url: '/busquedaMuestras',
      templateUrl: 'views/busquedamuestras.html',
      controller: 'BusquedamuestrasCtrl'
    })
    .state('recepcionMuestras', {
      url: '/recepcionMuestras',
      templateUrl: 'views/recepcionmuestras.html',
      controller: 'RecepcionmuestrasCtrl'
    })
    .state('listadoMuestras', {
      url: '/listadoMuestras',
      templateUrl: 'views/listadomuestras.html',
      controller: 'ListadomuestrasCtrl'
    })
    .state('registroMuestras', {
      url: '/registroMuestras',
      templateUrl: 'views/registromuestras.html',
      controller: 'RegistromuestrasCtrl'
    })
    .state('ingresarResultado', {
      url: '/ingresarResultado',
      templateUrl: 'views/ingresarresultado.html',
      controller: 'IngresarresultadoCtrl'
    })

    
   ;
  }]);
