'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:IngresarresultadoCtrl
 * @description
 * # IngresarresultadoCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('IngresarresultadoCtrl', ['$scope', '$http', '$state', 'dataManager', 'localStorageService', function ($scope, $http, $state, dataManager, localStorageService) {

    let user = localStorageService.get('user');
    if (user == null) {
      window.alert("inicie Sesión");
      $state.go('intranet');
    }
    else {
      $scope.message = "Usuario Activo = " + user.desc_categoria;
    }

    $scope.cerrarSesion = function () {
      localStorageService.set('user', null);
      $state.go('intranet');
    }

    $scope.volver = function () {
      if (user.categoria_id == 1) {
        $state.go('mainAdministrador');
      }
      else if (user.categoria_id == 2) {
        $state.go('mainReceptor');
      } else if (user.categoria_id == 3) {
        $state.go('mainTecnico');
      };
    }
    $scope.dato = localStorageService.get('dato');

    $scope.IngresarResultados = function (fechaResult, PPM) {
      let IDResult = $scope.dato.id_resultado;
      let rut_empleado = user.rut_empleado
      $http.post('http://localhost:3000/ingresarResultado', { IDResult, fechaResult, PPM, rut_empleado })
        .then((success) => {
          console.log("success.data" + success.data);
          window.alert("Resultado ingresado correctamente");
          $state.go('registroMuestras');
        }).catch((error) => {
          console.log("Error IngresarResultados " + error);
          $scope.message = "Problemas en conexion - Reintente ";
          return;
        });


    }

  }]);

