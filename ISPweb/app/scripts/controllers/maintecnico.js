'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:MaintecnicoCtrl
 * @description
 * # MaintecnicoCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('MaintecnicoCtrl',  [ '$scope', '$state','localStorageService', function ( $scope, $state ,localStorageService) {
    let user = localStorageService.get ('user');
    if (user == null )
    {
      window.alert("inicie Sesión");
      $state.go( 'intranet' );
    }
    else{
      $scope.message = "Usuario Activo = " + user.desc_categoria;
    }

    $scope.cerrarSesion = function(){
      localStorageService.set( 'user', null );
      $state.go( 'intranet' );
    }

    $scope.LoadListadoMuestras = function( ) {
      $state.go( 'listadoMuestras' );
    }
    $scope.LoadRegistroMuestras = function( ) {
      $state.go( 'registroMuestras' );
    }
  }]);
