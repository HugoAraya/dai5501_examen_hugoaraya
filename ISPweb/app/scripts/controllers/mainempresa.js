'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:MainempresaCtrl
 * @description
 * # MainempresaCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('MainempresaCtrl',[ '$scope', '$state','localStorageService', function ( $scope, $state ,localStorageService) {
    if (localStorageService.get ('user') == null )
    {
      window.alert("inicie Sesión");
      $state.go( 'intranet' );
    }
    else{
      let user = localStorageService.get ('user');
      $scope.message = "Usuario Activo = " + user.codigo;
    }

    $scope.cerrarSesion = function(){
      localStorageService.set( 'user', null );
      $state.go( 'intranet' );
    }

    $scope.LoadModificar = function( ) {
      $state.go( 'datosEmpresa' );
    }
    $scope.LoadResultados = function( ) {
      $state.go( 'resultados' );
    }
    $scope.LoadBusquedaMuestras = function( ) {
      $state.go( 'busquedaMuestras' );
    }
  }]);