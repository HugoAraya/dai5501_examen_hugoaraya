'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:ResultadosCtrl
 * @description
 * # ResultadosCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('ResultadosCtrl', ['$scope', '$http', '$state', 'dataManager', 'localStorageService', function ($scope, $http, $state, dataManager, localStorageService) {
    $scope.dato = localStorageService.get('dato');
    let ID_Recep = $scope.dato.id_recepcion;
    console.log("dato.id_recepcion" + $scope.dato.id_recepcion);
    console.log("ID_Recep--- " + ID_Recep);
    let label = [];
    let datar = [];
    $http.post('http://localhost:3000/getResultados', { ID_Recep })
      .then(function mySuccess(response) {
        console.log("response.data" + response.data);
        $scope.muestras = response.data;

        for (var i in $scope.muestras) {
          label.push($scope.muestras[i].det_tipoanalisis);
          
          datar.push($scope.muestras[i].PPM_resultado);
          
        }
        console.log("lab+dat"+label,datar)
        $scope.labels = label;
        $scope.data = datar;
    
      }, function myError(response) {
        console.log("response.statusText" + response.statusText);
      });

    



    // $scope.muestra = dataManager.getMuestra( );
    // $scope.labels = ["Microtoxinas", "Metales Pesados", "Plagicidas"];
    // $scope.data = [20, 30, 32];


  }]);
  // id_resultado: muestras.id_resultado,
  // tipo_id: muestras.tipo_id,
  // det_tipoanalisis: muestras.det_tipoanalisis,
  // desc_estado: muestras.desc_estado,
  // PPM_resultado: muestras.PPM_resultado,
  // codigo_recepcion: muestras.codigo_recepcion,
  // id_recepcion: muestras.id_recepcion