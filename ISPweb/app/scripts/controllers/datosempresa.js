'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:DatosempresaCtrl
 * @description
 * # DatosempresaCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('DatosempresaCtrl', [ '$scope', '$state','localStorageService', function ( $scope, $state ,localStorageService) {
    if (localStorageService.get ('user') == null )
    {
      window.alert("inicie Sesión");
      $state.go( 'intranet' );
    }
    else{
      let user = localStorageService.get ('user');
      $scope.message = "Usuario Activo = " + user.codigo;
    }

    $scope.cerrarSesion = function(){
      localStorageService.set( 'user', null );
      $state.go( 'intranet' );
    }
    
  }]);
