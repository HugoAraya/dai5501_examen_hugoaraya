'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:RegistromuestrasCtrl
 * @description
 * # RegistromuestrasCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('RegistromuestrasCtrl', ['$scope', '$http', '$state', 'dataManager', 'localStorageService', function ($scope, $http, $state, dataManager, localStorageService) {
    let user = localStorageService.get('user');
    if (user == null) {
      window.alert("inicie Sesión");
      $state.go('intranet');
    }
    else {
      $scope.message = "Usuario Activo = " + user.desc_categoria;
    }

    $scope.cerrarSesion = function () {
      localStorageService.set('user', null);
      $state.go('intranet');
    }

    $scope.volver = function () {
      if (user.categoria_id == 1) {
        $state.go('mainAdministrador');
      }
      else if (user.categoria_id == 2) {
        $state.go('mainReceptor');
      } else if (user.categoria_id == 3) {
        $state.go('mainTecnico');
      };
    }

    $scope.CodigoCliente = "";

    $http.get('http://localhost:3000/getMuestras')
      .then(function mySuccess(response) {
        console.log("response.data" + response.data);
        $scope.muestra = response.data;
      }, function myError(response) {
        console.log("response.statusText" + response.statusText);
      });

    $scope.filtrarMuestras = function (CodigoCliente) {
      console.log("CodigoCliente"+CodigoCliente);
      $http.post('http://localhost:3000/filtrarMuestras', { CodigoCliente })
        .then(function mySuccess(response) {
          console.log("response.data" + response.data);
          $scope.muestra = response.data;
          return;
        }, function myError(response) {
          console.log("response.statusText" + response.statusText);
          return;
        });
    }
    $scope.AgregarResultado = function (dato) {
      console.log(dato.id_resultado);
      localStorageService.set('dato', dato);
      $state.go('ingresarResultado');


    }

  }
  ]);

