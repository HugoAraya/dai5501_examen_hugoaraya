'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:MainadministradorCtrl
 * @description
 * # MainadministradorCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('MainadministradorCtrl', [ '$scope', '$state','localStorageService', function ( $scope, $state ,localStorageService) {
    let user = localStorageService.get ('user');
    if (user == null )
    {
      window.alert("inicie Sesión");
      $state.go( 'intranet' );
    }
    else{
      $scope.message = "Usuario Activo = " + user.desc_categoria;
    }

    $scope.cerrarSesion = function(){
      localStorageService.set( 'user', null );
      $state.go( 'intranet' );
    }

    $scope.LoadRecepcionMuestras = function( ) {
      $state.go( 'recepcionMuestras' );
    }
    $scope.LoadListadoMuestras = function( ) {
      $state.go( 'listadoMuestras' );
    }
    $scope.LoadRegistroMuestras = function( ) {
      $state.go( 'registroMuestras' );
    }
    $scope.LoadModificaDatos = function( ) {
      $state.go( 'datosCliente' );
    }

  }]);
