'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:MainreceptorCtrl
 * @description
 * # MainreceptorCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('MainreceptorCtrl', [ '$scope', '$state','localStorageService', function ( $scope, $state ,localStorageService) {
    let user = localStorageService.get ('user');
    if (user == null )
    {
      window.alert("inicie Sesión");
      $state.go( 'intranet' );
    }
    else{
      $scope.message = "Usuario Activo = " + user.desc_categoria;
    }

    $scope.cerrarSesion = function(){
      localStorageService.set( 'user', null );
      $state.go( 'intranet' );
    }

    $scope.LoadRecepcionMuestras = function( ) {
      $state.go( 'recepcionMuestras' );
    }
    $scope.LoadListadoMuestras = function( ) {
      $state.go( 'listadoMuestras' );
    }
  }]);
