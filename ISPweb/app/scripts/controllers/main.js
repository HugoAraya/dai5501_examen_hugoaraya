'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
