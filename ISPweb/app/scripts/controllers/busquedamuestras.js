'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:BusquedamuestrasCtrl
 * @description
 * # BusquedamuestrasCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('BusquedamuestrasCtrl', ['$scope', '$http', '$state', 'dataManager', 'localStorageService', function ($scope, $http, $state, dataManager, localStorageService) {
    let user = localStorageService.get('user');
    if (user == null) {
      window.alert("inicie Sesión");
      $state.go('intranet');
    }
    else {
      $scope.message = "Usuario Activo = " + user.codigo;
    }

    $scope.cerrarSesion = function () {
      localStorageService.set('user', null);
      $state.go('intranet');
    }

    let CodigoCliente = user.codigo;
    console.log("CodigoCliente"+CodigoCliente);
    $http.post('http://localhost:3000/filtrarMuestras2', { CodigoCliente })
      .then(function mySuccess(response) {
        console.log("response.data" + response.data);
        $scope.muestra = response.data;
        return;
      }, function myError(response) {
        console.log("response.statusText" + response.statusText);
        return;
      });

    $scope.VerResultado = function (dato) {
      console.log(dato.id_resultado);
      localStorageService.set('dato', dato);
      $state.go('resultados');
    }

  }
  ]);

