'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:RegistroclienteCtrl
 * @description
 * # RegistroclienteCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('RegistroclienteCtrl', ['$scope', '$http', '$state', function ($scope, $http, $state) {
    $scope.rut = "";
    $scope.password = "";
    $scope.nombre = "";
    $scope.direccion = "";
    $scope.telefono = "";
    $scope.mail = "";
    $scope.message = "";


    $scope.IngresoParticular = function (rut, password, nombre, direccion, email, telefono) {
      console.log(" rut " + rut);
      console.log(" email " + email);

      $http.post('http://localhost:3000/ingresoParticular', {
        rut, password, nombre, direccion, email, telefono
      })
        .then((success) => {
          console.log("success.data" + success.data);
          window.alert("Datos ingresados correctamente - Ingrese al sistema");
          $state.go('intranet');
        }).catch((error) => {
          $scope.message = "Rut Cliente Ya existe";
          console.log("Rut Cliente Ya existe");
          window.alert("Rut Cliente Ya existe - Reintente");
          return;
        });
    }


    $scope.nombreEmp = "";
    $scope.rutEmp = "";
    $scope.passwordEmp = "";
    $scope.direccionEmp = "";
    $scope.rutCon = "";
    $scope.nombreCon = "";
    $scope.emailCon = "";
    $scope.telefonoCon = "";

    $scope.IngresoEmpresa = function (nombreEmp, rutEmp, passwordEmp, direccionEmp, rutCon, nombreCon, emailCon, telefonoCon) {
      console.log(" nombreEmp " + nombreEmp);
      console.log(" rutCon " + rutCon);

      $http.post('http://localhost:3000/ingresoEmpresa', {
        nombreEmp, rutEmp, passwordEmp, direccionEmp, rutCon, nombreCon, emailCon, telefonoCon
      })
        .then((success) => {

          console.log("success.data" + success.data);
          //falta agregar Contacto
          $http.post('http://localhost:3000/ingresoContacto', {
            rutEmp, rutCon, nombreCon, emailCon, telefonoCon
          })
            .then((success) => {
                console.log("success.data ingresoContacto: " + success.data);
                
                window.alert("Datos ingresados correctamente - Ingrese al sistema");
                $state.go('intranet');
            }).catch((error) => {
              $scope.message = "Rut Empresa Ya existe - problema Contacto";
              console.log("Rut Empresa Ya existe - problema Contacto");
              window.alert("Rut Empresa Ya existe - problema Contacto");
              return;
            });
        
        }).catch((error) => {
          $scope.message = "Rut Empresa Ya existe";
          console.log("Rut Empresa Ya existe");
          window.alert("Rut Empresa Ya existe - Reintente");
          return;
        });
    }


  }])

