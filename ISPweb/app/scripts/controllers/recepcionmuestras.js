'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:RecepcionmuestrasCtrl
 * @description
 * # RecepcionmuestrasCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('RecepcionmuestrasCtrl', ['$scope', '$state', '$http', 'localStorageService', function ($scope, $state, $http, localStorageService) {
    let user = localStorageService.get('user');
    if (user == null) {
      window.alert("inicie Sesión");
      $state.go('intranet');
    }
    else {
      $scope.message = "Usuario Activo = " + user.desc_categoria;
    }

    $scope.cerrarSesion = function () {
      localStorageService.set('user', null);
      $state.go('intranet');
    }

    $scope.volver = function () {
      if (user.categoria_id == 1) {
        $state.go('mainAdministrador');
      }
      else if (user.categoria_id == 2) {
        $state.go('mainReceptor');
      } else if (user.categoria_id == 3) {
        $state.go('mainTecnico');
      };
    }


    $scope.codigoCli = "";
    $scope.fechaRecep = "";
    $scope.temperatura = "";
    $scope.cantidadMuestra = "";
    $scope.check1 = false;
    $scope.check2 = false;
    $scope.check3 = false;
    $scope.check4 = false;
    $scope.check5 = false;

    $scope.RecepcionarMuestra = function (codigoCli, fechaRecep, temperatura, cantidadMuestra, check1, check2, check3, check4, check5) {
      console.log("check1: " + check1);
      console.log("check2: " + check2);
      let confirmacion = 0;
      let Tipos = [];
      if (check1) {
        Tipos.push({ tipo: 1 });
      }
      if (check2) {
        Tipos.push({ tipo: 2 });
      }
      if (check3) {
        Tipos.push({ tipo: 3 });
      }
      if (check4) {
        Tipos.push({ tipo: 4 });
      }
      if (check5) {
        Tipos.push({ tipo: 5 });
      }
      console.log("Tipos.length: " + Tipos.length);

      if (Tipos.length <= 0) {
        window.alert("Seleccione Tipos de Examenes");
        return;
      }

      //RUT EMPLEADO
      let rut_empleado = user.rut_empleado;
      let ID_Recep = "";

      //validar codigo cliente
      let estado = 0;
      $http.post('http://localhost:3000/validarCodigoCLiente', { codigoCli })
        .then((success) => {
          console.log("success.data" + success.data);
          estado = success.data;
          console.log("estado1" + estado);


          $http.post('http://localhost:3000/validarCodigoCLiente2', { codigoCli })
            .then((success) => {
              console.log("success.data" + success.data);
              if ( estado == 0 ){
                estado = success.data;
              }
              console.log("estado2" + estado);


              //Ingresar Recepcion
              console.log("estado3" + estado);
              if (estado == 1) {
                $http.post('http://localhost:3000/ingresarRecepcion', { codigoCli, fechaRecep, temperatura, cantidadMuestra, rut_empleado })
                  .then((success) => {
                    console.log("success.data" + success.data);
                    //Ultimo ID Recepcion        
                    $http.get('http://localhost:3000/get_ultimo_id_recep')
                      .then((success) => {
                        ID_Recep = success.data;
                        console.log("RECEPPP :" + ID_Recep);
                        //Generar Registros para Resultados

                        for (var i in Tipos) {
                          let tipo = Tipos[i].tipo;
                          console.log("Tipos RECEPPP :" + ID_Recep);
                          $http.post('http://localhost:3000/ingresarRegistro', { tipo, ID_Recep })
                            .then((success) => {
                              console.log("success.data" + success.data);

                            }).catch((error) => {
                              console.log("Error IngresarRegistro " + error);
                              $scope.message = "Problemas en conexion - Reintente ";
                              return;
                            });
                        }
                        window.alert("Recepcion ingresada correctamente");
                        if (user.categoria_id == 1) {
                          $state.go('mainAdministrador');
                        }
                        else if (user.categoria_id == 2) {
                          $state.go('mainReceptor');
                        } else if (user.categoria_id == 3) {
                          $state.go('mainTecnico');
                        };
                      });


                  }).catch((error) => {
                    console.log("Error ingresarRecepcion " + error);
                    $scope.message = "Problemas en conexion - Reintente ";
                    return;
                  });
              } else {
                console.log("Codigo Cliente No existe");
                $scope.message = "Codigo Cliente No existe - Reintente ";
                window.alert("Codigo Cliente No existe - Reintente");
                return;
              }





            }).catch((error) => {
              console.log("Codigo Cliente No existe 2" + error);
              $scope.message = "Problemas en conexion - Reintente ";
              return;
            });



          //window.alert("Codigo validado Correctamente " + success.data);
        }).catch((error) => {
          console.log("Codigo Cliente No existe " + error);
          $scope.message = "Problemas en conexion - Reintente ";
          return;
        });





    }

  }]);
