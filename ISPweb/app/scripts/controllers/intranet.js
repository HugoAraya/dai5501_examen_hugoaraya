'use strict';

/**
 * @ngdoc function
 * @name ispwebApp.controller:IntranetCtrl
 * @description
 * # IntranetCtrl
 * Controller of the ispwebApp
 */
angular.module('ispwebApp')
  .controller('IntranetCtrl', ['$scope', '$state', '$http', 'localStorageService', function ($scope, $state, $http, localStorageService) {
    $scope.rut = "";
    $scope.password = "";

    $scope.LoadMainCliente = function (rut, password) {
      console.log("El password " + password);
      console.log("El rut " + rut);
      $http.post('http://localhost:3000/loginParticular', { rut, password })
        .then((success) => {
          console.log("success.data[0] " + success.data[0]);
          let user = success.data[0];
          localStorageService.set('user', user);
          console.log("user " + user.id_particular);
          if (user == null) {
            console.log("if (user == null) ");
            $scope.message = "Contraseña o username no corresponden";
            return;
          } else {
            $state.go('mainCliente');
          };

        }).catch((error) => {
          console.log("Contraseña o username no corresponden");
          $scope.message = "Contraseña o username no corresponden";
          console.log("error " + error);
        });

    }
    $scope.LoadMainEmpresa = function (rutEmp, passwordEmp) {
      console.log("El passwordEmp " + passwordEmp);
      console.log("El rutEmp " + rutEmp);
      $http.post('http://localhost:3000/loginEmpresa', { rutEmp, passwordEmp })
        .then((success) => {
          console.log("success.data[0] " + success.data[0]);
          let user = success.data[0];
          localStorageService.set('user', user);
          console.log("user " + user.codigo);
          if (user == null) {
            console.log("if (user == null) ");
            $scope.message = "Contraseña o username no corresponden";
            return;
          } else {
            $state.go('mainEmpresa');
          };

        }).catch((error) => {
          console.log("Contraseña o username no corresponden");
          $scope.message = "Contraseña o username no corresponden";
          console.log("error " + error);
        });
    }



    $scope.LoadMainEmpleado = function (rutEmple, passwordEmple) {
      console.log("El passwordEmp " + passwordEmple);
      console.log("El rutEmp " + rutEmple);
      $http.post('http://localhost:3000/loginEmpleado', { rutEmple, passwordEmple })
        .then((success) => {
          console.log("success.data[0] " + success.data[0]);
          let user = success.data[0];
          localStorageService.set('user', user);
          console.log("categoria_id " + user.categoria_id);
          if (user == null) {
            console.log("if (user == null) ");
            $scope.message = "Contraseña o username no corresponden";
            return;
          }

          if (user.categoria_id == 1) {
            $state.go('mainAdministrador');
          }
          else if (user.categoria_id == 2) {
            $state.go('mainReceptor');
          } else if (user.categoria_id == 3) {
            $state.go('mainTecnico');
          };

        }).catch((error) => {
          console.log("Contraseña o username no corresponden");
          $scope.message = "Contraseña o username no corresponden";
          console.log("error " + error);
        });

    }
  }]);