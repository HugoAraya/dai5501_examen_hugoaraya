'use strict';

/**
 * @ngdoc service
 * @name ispwebApp.DataManager
 * @description
 * # DataManager
 * Service in the ispwebApp.
 */
angular.module('ispwebApp')
.service('dataManager', function () {
	this.muestra = { };
	
	this.setMuestra = function( muestra ) {
		this.muestra = muestra;
	}
	
	this.getMuestra = function( ) {
		return this.muestra;
	}
  });
