'use strict';

describe('Service: validarRut', function () {

  // load the service's module
  beforeEach(module('ispwebApp'));

  // instantiate service
  var validarRut;
  beforeEach(inject(function (_validarRut_) {
    validarRut = _validarRut_;
  }));

  it('should do something', function () {
    expect(!!validarRut).toBe(true);
  });

});
