'use strict';

describe('Controller: RecepcionmuestrasCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var RecepcionmuestrasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RecepcionmuestrasCtrl = $controller('RecepcionmuestrasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RecepcionmuestrasCtrl.awesomeThings.length).toBe(3);
  });
});
