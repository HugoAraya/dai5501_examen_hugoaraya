'use strict';

describe('Controller: RegistroclienteCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var RegistroclienteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RegistroclienteCtrl = $controller('RegistroclienteCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RegistroclienteCtrl.awesomeThings.length).toBe(3);
  });
});
