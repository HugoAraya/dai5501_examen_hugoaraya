'use strict';

describe('Controller: ListadomuestrasCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var ListadomuestrasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ListadomuestrasCtrl = $controller('ListadomuestrasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ListadomuestrasCtrl.awesomeThings.length).toBe(3);
  });
});
