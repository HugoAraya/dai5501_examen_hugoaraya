'use strict';

describe('Controller: DatosempresaCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var DatosempresaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DatosempresaCtrl = $controller('DatosempresaCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DatosempresaCtrl.awesomeThings.length).toBe(3);
  });
});
