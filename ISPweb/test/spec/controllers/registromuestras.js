'use strict';

describe('Controller: RegistromuestrasCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var RegistromuestrasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RegistromuestrasCtrl = $controller('RegistromuestrasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(RegistromuestrasCtrl.awesomeThings.length).toBe(3);
  });
});
