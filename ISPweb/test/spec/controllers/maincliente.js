'use strict';

describe('Controller: MainclienteCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var MainclienteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainclienteCtrl = $controller('MainclienteCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MainclienteCtrl.awesomeThings.length).toBe(3);
  });
});
