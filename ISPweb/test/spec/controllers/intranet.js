'use strict';

describe('Controller: IntranetCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var IntranetCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IntranetCtrl = $controller('IntranetCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(IntranetCtrl.awesomeThings.length).toBe(3);
  });
});
