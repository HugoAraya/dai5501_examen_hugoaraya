'use strict';

describe('Controller: BusquedamuestrasCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var BusquedamuestrasCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    BusquedamuestrasCtrl = $controller('BusquedamuestrasCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(BusquedamuestrasCtrl.awesomeThings.length).toBe(3);
  });
});
