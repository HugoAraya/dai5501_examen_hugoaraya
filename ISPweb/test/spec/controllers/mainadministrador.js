'use strict';

describe('Controller: MainadministradorCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var MainadministradorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainadministradorCtrl = $controller('MainadministradorCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MainadministradorCtrl.awesomeThings.length).toBe(3);
  });
});
