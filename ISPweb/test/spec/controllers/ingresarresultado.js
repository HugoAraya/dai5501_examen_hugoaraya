'use strict';

describe('Controller: IngresarresultadoCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var IngresarresultadoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    IngresarresultadoCtrl = $controller('IngresarresultadoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(IngresarresultadoCtrl.awesomeThings.length).toBe(3);
  });
});
