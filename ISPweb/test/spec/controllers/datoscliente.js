'use strict';

describe('Controller: DatosclienteCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var DatosclienteCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    DatosclienteCtrl = $controller('DatosclienteCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(DatosclienteCtrl.awesomeThings.length).toBe(3);
  });
});
