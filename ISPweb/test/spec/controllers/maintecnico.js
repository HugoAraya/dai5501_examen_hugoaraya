'use strict';

describe('Controller: MaintecnicoCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var MaintecnicoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MaintecnicoCtrl = $controller('MaintecnicoCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MaintecnicoCtrl.awesomeThings.length).toBe(3);
  });
});
