'use strict';

describe('Controller: MainreceptorCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var MainreceptorCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainreceptorCtrl = $controller('MainreceptorCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MainreceptorCtrl.awesomeThings.length).toBe(3);
  });
});
