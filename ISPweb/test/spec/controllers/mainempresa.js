'use strict';

describe('Controller: MainempresaCtrl', function () {

  // load the controller's module
  beforeEach(module('ispwebApp'));

  var MainempresaCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MainempresaCtrl = $controller('MainempresaCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(MainempresaCtrl.awesomeThings.length).toBe(3);
  });
});
