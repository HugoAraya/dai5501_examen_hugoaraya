const obtenerDao = require("../dao/obtener.js");

const getcategorias = function(req, res) {
    obtenerDao.getcategorias().then((success) => {
        res.send(success);
    }).catch((error) => {
        res.status(500).send(error);
    });
}

const get_ultimo_id_recep = function(req, res) {
    obtenerDao.get_ultimo_id_recep()  
    .then((success) => {
        console.log("business" + success);
        res.send(success.toString());       
    }).catch((error) => {
        res.status(500).send(error);
    });
}





const loginParticular = function(req, res) {
    console.log("req.body.rut; "+req.body.rut);
    console.log("req.body.password; "+req.body.password);
    let rut = req.body.rut;
    let password = req.body.password;
    obtenerDao.loginParticular(rut,password).then((success) => {
        console.log("success businne: " +success);
        res.send(success);
    }).catch((error) => {
        console.log("error businne: " +error);
        res.status(500).send(error);
    });
}

const loginEmpresa = function(req, res) {
    console.log("req.body.rutEmp; "+req.body.rutEmp);
    console.log("req.body.passwordEmp; "+req.body.passwordEmp);
    let rutEmp = req.body.rutEmp;
    let passwordEmp = req.body.passwordEmp;
    obtenerDao.loginEmpresa(rutEmp,passwordEmp).then((success) => {
        console.log("success businne: " +success);
        res.send(success);
    }).catch((error) => {
        console.log("error businne: " +error);
        res.status(500).send(error);
    });
}

const loginEmpleado = function(req, res) {
    console.log("req.body.rutEmple; "+req.body.rutEmple);
    console.log("req.body.passwordEmple; "+req.body.passwordEmple);
    let rutEmple = req.body.rutEmple;
    let passwordEmple = req.body.passwordEmple;
    obtenerDao.loginEmpleado(rutEmple,passwordEmple).then((success) => {
        console.log("success businne: " +success);
        res.send(success);
    }).catch((error) => {
        console.log("error businne: " +error);
        res.status(500).send(error);
    });
}


const getMuestras = function(req, res) {
    obtenerDao.getMuestras().then((success) => {
        res.send(success);
    }).catch((error) => {
        res.status(500).send(error);
    });
}

const filtrarMuestras = function(req, res) {
    let CodigoCliente = req.body.CodigoCliente;
    console.log("CodigoCliente"+CodigoCliente);
    obtenerDao.filtrarMuestras(CodigoCliente ).then((success) => {
        res.send(success);
    }).catch((error) => {
        res.status(500).send(error);
    });
}

const getMuestras2 = function(req, res) {
    obtenerDao.getMuestras2().then((success) => {
        res.send(success);
    }).catch((error) => {
        res.status(500).send(error);
    });
}

const getResultados = function(req, res) {
    let ID_Recep = req.body.ID_Recep;
    console.log("ID_Recep -- "+ ID_Recep);
    console.log("req.body.ID_Recep -- "+ req.body.ID_Recep);
    obtenerDao.getResultados(ID_Recep).then((success) => {
        console.log("business getResultados" + success);
        res.send(success);
    }).catch((error) => {
        res.status(500).send(error);
    });
}

const filtrarMuestras2 = function(req, res) {
    let CodigoCliente = req.body.CodigoCliente;
    console.log("CodigoCliente"+CodigoCliente);
    obtenerDao.filtrarMuestras2(CodigoCliente ).then((success) => {
        res.send(success);
    }).catch((error) => {
        res.status(500).send(error);
    });
}



module.exports.getcategorias = getcategorias;
module.exports.loginParticular = loginParticular;
module.exports.loginEmpresa = loginEmpresa;
module.exports.loginEmpleado = loginEmpleado;
module.exports.get_ultimo_id_recep = get_ultimo_id_recep;
module.exports.getMuestras = getMuestras;
module.exports.filtrarMuestras = filtrarMuestras;
module.exports.getMuestras2 = getMuestras2;
module.exports.getResultados = getResultados;
module.exports.filtrarMuestras2 = filtrarMuestras2;
