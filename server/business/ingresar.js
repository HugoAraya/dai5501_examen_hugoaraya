const ingresarDao = require("../dao/ingresar.js");



const ingresoParticular = function (req, res) {
    let rut = req.body.rut;
    let password = req.body.password;
    let nombre = req.body.nombre;
    let direccion = req.body.direccion;
    let email = req.body.email;
    let telefono = req.body.telefono;

    ingresarDao.revisarParticular(rut).then((success) => {
        console.log(success);
        if (success == 0) {
            ingresarDao.ingresoParticular(rut, password, nombre, direccion, email, telefono)
                .then((success) => {
                    console.log("success " + success);
                    res.send(success);
                })
                .catch((error) => {
                    console.log("error en business - ingresoParticular");
                    res.status(500).send(error);
                });
        } else {
            res.status(401).send("Ya existe Rut");
        }
    }).catch((error) => {
        res.status(500).send(error);
    });

}




//ingresoEmpresa = function (nombreEmp, rutEmp, passwordEmp, direccionEmp, rutCon, nombreCon, emailCon, telefonoCon) {

const ingresoEmpresa = function (req, res) {
    let nombreEmp = req.body.nombreEmp;
    let rutEmp = req.body.rutEmp;
    let passwordEmp = req.body.passwordEmp;
    let direccionEmp = req.body.direccionEmp;
    let rutCon = req.body.rutCon;
    let nombreCon = req.body.nombreCon;
    let emailCon = req.body.emailCon;
    let telefonoCon = req.body.telefonoCon;


    ingresarDao.revisarEmpresa(rutEmp).then((success) => {
        console.log(success);
        if (success == 0) {
            ingresarDao.ingresoEmpresa(nombreEmp, rutEmp, passwordEmp, direccionEmp)
                .then((success) => {
                    console.log("success " + success);
                    res.send(success);
                })
                .catch((error) => {
                    console.log("error en business - ingresoEmpresa");
                    res.status(500).send(error);
                });
        } else {
            res.status(401).send("Ya existe Rut");
        }
    }).catch((error) => {
        res.status(500).send(error);
    });

}

const ingresoContacto = function (req, res) {
    let rutEmp = req.body.rutEmp;
    let rutCon = req.body.rutCon;
    let nombreCon = req.body.nombreCon;
    let emailCon = req.body.emailCon;
    let telefonoCon = req.body.telefonoCon;
    ingresarDao.getIdEmpresaxRut(rutEmp)
        .then((IdEmpresa) => {
            console.log("IdEmpresa:" + IdEmpresa);
            console.log("IdEmpresa.toString(): " + IdEmpresa.toString());

            console.log("datos" + " " + rutCon + " " + nombreCon + " " + emailCon + " " + telefonoCon + " " + IdEmpresa)
            if (IdEmpresa != null) {
                ingresarDao.ingresoContacto(rutCon, nombreCon, emailCon, telefonoCon, IdEmpresa)
                    .then((success) => {
                        console.log("success " + success);
                        res.send(success);
                    })
                    .catch((error) => {
                        console.log("error en business - ingresoContacto");
                        res.status(500).send(error);
                    });
            } else {
                res.status(401).send("Error de Registro de empresa");
            }

        })
        .catch((error) => {
            res.status(500).send(error);
        });

}


const validarCodigoCLiente = function (req, res) {
    let codigoCli = req.body.codigoCli;
    ingresarDao.revisarCodigo( codigoCli )
        .then((success) => {
            console.log("success validar: " + success );
            res.send(String(success));
        })
        .catch((error) => {
            console.log("error en business - validarCodigoCLiente");
            res.status(500).send(error);
        });
}

const validarCodigoCLiente2 = function (req, res) {
    let codigoCli = req.body.codigoCli;
    ingresarDao.revisarCodigo2(codigoCli)
        .then((success) => {
            console.log("success revisar codigo2_: " + success);
            res.send(String(success));
        }).catch((error) => {
            res.status(500).send(error);
        });
}

const ingresarRecepcion = function (req, res) {
    let codigoCli = req.body.codigoCli;
    let fechaRecep = req.body.fechaRecep;
    let temperatura = req.body.temperatura;
    let cantidadMuestra = req.body.cantidadMuestra;
    let rut_empleado = req.body.rut_empleado;

    ingresarDao.ingresarRecepcion(fechaRecep, temperatura, cantidadMuestra, codigoCli, rut_empleado)
        .then((success) => {
            console.log("success " + success);
            res.send(success);
        })
        .catch((error) => {
            console.log("error en business - ingresarRecepcion");
            res.status(500).send(error);
        });
}
const ingresarRegistro = function (req, res) {
    let tipo = req.body.tipo;
    let ID_Recep = req.body.ID_Recep;

    ingresarDao.ingresarRegistro(tipo, ID_Recep)
        .then((success) => {
            console.log("success " + success);
            res.send(success);
        })
        .catch((error) => {
            console.log("error en business - ingresarRegistro");
            res.status(500).send(error);
        });
}


const ingresarResultado = function (req, res) {
    let IDResult = req.body.IDResult;
    let fechaResult = req.body.fechaResult;
    let PPM = req.body.PPM;
    let rut_empleado = req.body.rut_empleado;

    ingresarDao.ingresarResultado(IDResult,fechaResult, PPM, rut_empleado )
        .then((success) => {
            console.log("success " + success);
            res.send(success);
        })
        .catch((error) => {
            console.log("error en business - ingresarRegistro");
            res.status(500).send(error);
        });
}





module.exports.ingresoParticular = ingresoParticular;
module.exports.ingresoEmpresa = ingresoEmpresa;
module.exports.ingresoContacto = ingresoContacto;
module.exports.ingresarRecepcion = ingresarRecepcion;
module.exports.validarCodigoCLiente = validarCodigoCLiente;
module.exports.validarCodigoCLiente2 = validarCodigoCLiente2;
module.exports.ingresarRegistro = ingresarRegistro;
module.exports.ingresarResultado = ingresarResultado;