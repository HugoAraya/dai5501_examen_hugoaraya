const obtener = require("../business/obtener.js");
const ingresar = require("../business/ingresar");

module.exports = function(app) {
    app.get("/", function(req, res) {
        res.send("Prueba NODE SERVER HUGO - OK");
    });

    app.get("/getCategorias", function(req, res) {
        obtener.getcategorias(req, res);
    });
    app.post("/ingresoParticular", function(req, res) {
        ingresar.ingresoParticular(req, res);
    });
    app.post("/ingresoEmpresa", function(req, res) {
        ingresar.ingresoEmpresa(req, res);
    });
    app.post("/ingresoContacto", function(req, res) {
        ingresar.ingresoContacto(req, res);
    });
    app.post("/loginParticular", function(req, res) {
        obtener.loginParticular(req, res);
    });
    app.post("/loginEmpresa", function(req, res) {
        obtener.loginEmpresa(req, res);
    });
    app.post("/loginEmpleado", function(req, res) {
        obtener.loginEmpleado(req, res);
    });
    app.post("/validarCodigoCLiente", function(req, res) {
        ingresar.validarCodigoCLiente(req, res);
    });
    app.post("/validarCodigoCLiente2", function(req, res) {
        ingresar.validarCodigoCLiente2(req, res);
    });    
    app.post("/ingresarRecepcion", function(req, res) {
        ingresar.ingresarRecepcion(req, res);
    });
    app.get("/get_ultimo_id_recep", function(req, res) {
        obtener.get_ultimo_id_recep(req, res);
    });
    app.post("/ingresarRegistro", function(req, res) {
        ingresar.ingresarRegistro(req, res);
    });
    app.get("/getMuestras", function(req, res) {
        obtener.getMuestras(req, res);
    });

    app.post("/filtrarMuestras", function(req, res) {
        obtener.filtrarMuestras(req, res);
    });
    app.post("/ingresarResultado", function(req, res) {
        ingresar.ingresarResultado(req, res);
    });
    app.get("/getMuestras2", function(req, res) {
        obtener.getMuestras2(req, res);
    });

    app.post("/getResultados", function(req, res) {
        obtener.getResultados(req, res);
    });

    app.post("/filtrarMuestras2", function(req, res) {
        obtener.filtrarMuestras2(req, res);
    });    
}