const con = require("../dao/connection.js");


const getcategorias = function() {
    let query = "CAll GET_CATEGORIA()";
    return new Promise((resolve, reject) => {
        con.query(query, [], function(error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            
            result = result[0].map(function(categoria) {
                let output = {
                    id: categoria.id_categoria,
                    desc_categoria: categoria.desc_categoria
                };
                console.log("output"+output.desc_categoria +"  "+ output.id);
                return output
            });
            console.log(result);
            resolve(result);
        });
    });
}


const get_ultimo_id_recep = function() {
    let query = "CAll GET_ULTIMO_ID_RECEP()";
    return new Promise((resolve, reject) => {
        con.query(query, [], function(error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            console.log("REcepcion result ", result);
            let keys = Object.keys(result[0]);
            let RecepID = result[0][keys[0]];
            console.log("REcepcion ultima ", RecepID.ID);
            resolve(RecepID.ID);
        });
    });
}








const loginParticular = function(rut, password) {
    let query = "CAll GET_LOGIN_PARTICULAR(?,?)";
    return new Promise((resolve, reject) => {
        con.query(query, [rut, password], function(error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            result = result[0].map(function(particular) {
                let output = {
                    id_particular: particular.id_particular,
                    codigo: particular.codigo,
                    rut_particular: particular.rut_particular,
                    pasword_particular: particular.pasword_particular,
                    nombre_particular: particular.nombre_particular,
                    direccion_particular: particular.direccion_particular,
                    fono_particular: particular.fono_particular,
                    email_particular: particular.email_particular
                };
                console.log("output" + output.codigo);
                return output;
            });
            console.log("result"+result);
            resolve(result);
        });
    });
}


const loginEmpresa = function(rutEmp, passwordEmp) {
    let query = "CAll GET_LOGIN_EMPRESA(?,?)";
    return new Promise((resolve, reject) => {
        con.query(query, [rutEmp, passwordEmp], function(error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            result = result[0].map(function(empresa) {
                let output = {
                    id_empresa: empresa.id_empresa,
                    codigo: empresa.codigo,
                    rut_empresa: empresa.rut_empresa,
                    nombre_empresa: empresa.nombre_empresa,
                    password_empresa: empresa.password_empresa,
                    direccion_empresa: empresa.direccion_empresa
                };
                console.log("output" + output.codigo);
                return output;
            });
            console.log("result"+result);
            resolve(result);
        });
    });
}


const loginEmpleado = function(rutEmple, passwordEmple) {
    let query = "CAll GET_LOGIN_EMPLEADO(?,?)";
    return new Promise((resolve, reject) => {
        con.query(query, [rutEmple, passwordEmple], function(error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            result = result[0].map(function(empleado) {
                let output = {
                    id_empleado: empleado.id_empleado,
                    rut_empleado: empleado.rut_empleado,
                    nombre_empleado: empleado.nombre_empleado,
                    password_empleado: empleado.password_empleado,
                    categoria_id: empleado.categoria_id,
                    desc_categoria: empleado.desc_categoria

                };
                console.log("output" + output.desc_categoria);
                return output;
            });
            console.log("result"+result);
            resolve(result);
        });
    });
}


const getMuestras = function() {
    let query = "CAll GET_MUESTRAS1()";
    return new Promise((resolve, reject) => {
        con.query(query, [], function(error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            
            result = result[0].map(function(muestras) {
                let output = {
                    id_resultado: muestras.id_resultado,
                    tipo_id: muestras.tipo_id,
                    det_tipoanalisis: muestras.det_tipoanalisis,
                    desc_estado: muestras.desc_estado,
                    codigo_recepcion: muestras.codigo_recepcion,
                    id_recepcion: muestras.id_recepcion
                };
                console.log("output"+output.id_resultado +"  "+ output.tipo_id);
                return output
            });
            console.log(result);
            resolve(result);
        });
    });
}

const filtrarMuestras = function(CodigoCliente) {
    let query = "CAll GET_FILTRAR_MUESTRAS1(?)";
    return new Promise((resolve, reject) => {
        con.query(query, [CodigoCliente], function(error, result, fields) {
            if (error) {
                console.log("output error"+error); 
                reject(error);
                return;
            }            
            result = result[0].map(function(muestras) {
                let output = {
                    id_resultado: muestras.id_resultado,
                    tipo_id: muestras.tipo_id,
                    det_tipoanalisis: muestras.det_tipoanalisis,
                    desc_estado: muestras.desc_estado,
                    codigo_recepcion: muestras.codigo_recepcion,
                    id_recepcion: muestras.id_recepcion
                };
                console.log("output"+output.id_resultado +"  "+ output.tipo_id);
                return output
            });
            console.log(result);
            resolve(result);
        });
    });
}

const getMuestras2 = function() {
    let query = "CAll GET_MUESTRAS2()";
    return new Promise((resolve, reject) => {
        con.query(query, [], function(error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            
            result = result[0].map(function(muestras) {
                let output = {
                    id_resultado: muestras.id_resultado,
                    tipo_id: muestras.tipo_id,
                    det_tipoanalisis: muestras.det_tipoanalisis,
                    desc_estado: muestras.desc_estado,
                    codigo_recepcion: muestras.codigo_recepcion,
                    id_recepcion: muestras.id_recepcion
                };
                console.log("output"+output.id_resultado +"  "+ output.tipo_id);
                return output
            });
            console.log(result);
            resolve(result);
        });
    });
}

const getResultados = function(ID_Recep) {
    console.log("ID_Recep --- "+ID_Recep)
    let query = "CAll GET_RESSULTADO(?)";
    return new Promise((resolve, reject) => {
        con.query(query, [ ID_Recep ] , function(error, result, fields) {
            if (error) {
                console.log("error GET_RESSULTADO"+error)
                reject(error);
                return;
            }
            
            result = result[0].map(function(muestras) {
                let output = {
                    id_resultado: muestras.id_resultado,
                    tipo_id: muestras.tipo_id,
                    det_tipoanalisis: muestras.det_tipoanalisis,
                    desc_estado: muestras.desc_estado,
                    PPM_resultado: muestras.PPM_resultado,
                    codigo_recepcion: muestras.codigo_recepcion,
                    id_recepcion: muestras.id_recepcion
                };
                console.log("output"+output.id_resultado +"  "+ output.PPM_resultado);
                return output;
            });
            console.log(result);
            resolve(result);
        });
    });
}

const filtrarMuestras2 = function(CodigoCliente) {
    let query = "CAll GET_FILTRAR_MUESTRAS2(?)";
    return new Promise((resolve, reject) => {
        con.query(query, [CodigoCliente], function(error, result, fields) {
            if (error) {
                console.log("output error"+error); 
                reject(error);
                return;
            }            
            result = result[0].map(function(muestras) {
                let output = {
                    id_resultado: muestras.id_resultado,
                    tipo_id: muestras.tipo_id,
                    det_tipoanalisis: muestras.det_tipoanalisis,
                    desc_estado: muestras.desc_estado,
                    codigo_recepcion: muestras.codigo_recepcion,
                    id_recepcion: muestras.id_recepcion
                };
                console.log("output"+output.id_resultado +"  "+ output.tipo_id);
                return output
            });
            console.log(result);
            resolve(result);
        });
    });
}

module.exports.getcategorias = getcategorias;
module.exports.loginParticular = loginParticular;
module.exports.loginEmpresa = loginEmpresa;
module.exports.loginEmpleado = loginEmpleado;
module.exports.get_ultimo_id_recep = get_ultimo_id_recep;
module.exports.getMuestras = getMuestras;
module.exports.filtrarMuestras = filtrarMuestras;
module.exports.getMuestras2 = getMuestras2;
module.exports.getResultados = getResultados;
module.exports.filtrarMuestras2 = filtrarMuestras2;