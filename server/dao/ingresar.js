const con = require("../dao/connection.js");


const revisarParticular = function (rut) {
    let query1 = "SELECT GetIDParticular( ? )";
    return new Promise((resolve, reject) => {
        con.query(query1, [rut], function (error, result, fields) {
            if (error) {
                console.log("GetIDParticular error" + error);
                reject(error);
                return;
            }
            // NO hay errores, asi que podemos saber si se logueó o no
            let keys = Object.keys(result[0]);
            console.log("keys(result[0])" + Object.keys(result[0]));
            resolve(result[0][keys[0]]);
        });
    });

}
const ingresoParticular = function (rut, password, nombre, direccion, email, telefono) {
    let codigo_particular = "PERSO" + rut.substring(1, 4);
    let query = "SELECT CREATE_PARTICULAR (?,?,?,?,?,?,?,?)";
    return new Promise((resolve, reject) => {
        con.query(query, [0, codigo_particular, rut, password, nombre, direccion, telefono, email], function (error, result, fields) {
            if (error) {
                console.log("dao CREATE_PARTICULAR")
                reject(error);
                return;
            }
            console.log(result);
            resolve(result);
        });
    });

}



const revisarEmpresa = function (rutEmp) {
    let query3 = "SELECT GetIDEmpresa( ? )";
    return new Promise((resolve, reject) => {
        con.query(query3, [rutEmp], function (error, result, fields) {
            if (error) {
                console.log("GetIDParticular error" + error);
                reject(error);
                return;
            }
            // NO hay errores, asi que podemos saber si se logueó o no
            let keys = Object.keys(result[0]);
            console.log("keys(result[0])" + Object.keys(result[0]));
            resolve(result[0][keys[0]]);
        });
    });

}


const ingresoEmpresa = function (nombreEmp, rutEmp, passwordEmp, direccionEmp) {
    let codigo_empresa = "EMPRE" + rutEmp.substring(1, 4);
    let query4 = "SELECT CREATE_EMPRESA (?,?,?,?,?,?)";
    return new Promise((resolve, reject) => {
        con.query(query4, [0, codigo_empresa, rutEmp, nombreEmp, passwordEmp, direccionEmp], function (error, result, fields) {
            if (error) {
                console.log("dao error CREATE_EMPRESA")
                reject(error);
                return;
            }
            console.log(result);
            resolve(result);
        });
    });

}


const getIdEmpresaxRut = function (rutEmp) {
    let query5 = "SELECT `GET_ID_EMPRESA_X_RUT`( ? )";
    return new Promise((resolve, reject) => {
        con.query(query5, [rutEmp], function (error, result, fields) {
            if (error) {
                reject(error);
                return;
            }
            let keys = Object.keys(result[0]);
            let IdEmpresa = result[0][keys[0]];

            resolve(IdEmpresa);
        });
    });
}


const ingresoContacto = function (rutCon, nombreCon, emailCon, telefonoCon, IdEmpresa) {
    let query6 = "SELECT CREATE_CONTACTO (?,?,?,?,?,?)";
    return new Promise((resolve, reject) => {
        con.query(query6, [0, rutCon, nombreCon, emailCon, telefonoCon, IdEmpresa], function (error, result, fields) {
            if (error) {
                console.log("dao error CREATE_CONTACTO!!!")
                reject(error);
                return;
            }
            console.log(result);
            resolve(result);
        });
    });

}

const revisarCodigo = function (codigoCli) {
    let query7 = "SELECT GetIDCodigoParticular( ? )";
    return new Promise((resolve, reject) => {
        con.query(query7, [codigoCli], function (error, result) {
            if (error) {
                reject(error);
                return;
            }
            // NO hay errores, asi que podemos saber si se logueó o no
            let keys = Object.keys(result[0]);
            console.log("objeto: " + Object.keys(result[0]));
            console.log("objeto2: " + result[0][keys[0]]);
            resolve(result[0][keys[0]]);
        });
    });
}


const revisarCodigo2 = function (codigoCli) {
    let query8 = "SELECT GetIDCodigoEmpresa( ? )";
    return new Promise((resolve, reject) => {
        con.query(query8, [codigoCli], function (error, result) {
            if (error) {
                console.log("GetIDCodigoEmpresa error" + error);
                reject(error);
                return;
            }
            // NO hay errores, asi que podemos saber si se logueó o no
            let keys = Object.keys(result[0]);
            console.log("keys(result[]2221" + Object.keys(result[0]));
            resolve(result[0][keys[0]]);
        });
    });
}


const ingresarRecepcion = function (fechaRecep, temperatura, cantidadMuestra, codigoCli, rut_empleado) {
    let query6 = "SELECT CREATE_RECEPCION (?,?,?,?,?,?)";
    return new Promise((resolve, reject) => {
        con.query(query6, [0, fechaRecep, temperatura, cantidadMuestra, codigoCli, rut_empleado], function (error, result, fields) {
            if (error) {
                console.log("dao error CREATE_RECEPCION!!!")
                reject(error);
                return;
            }
            console.log(result);
            resolve(result);
        });
    });
}

const ingresarRegistro = function (tipo, ID_Recep) {
    console.log("function ( tipo, ID_Recep ) " + tipo + " " + ID_Recep);
    let query6 = "SELECT CREATE_REGISTRO (?,?,?,?,?,?,?)";

    return new Promise((resolve, reject) => {
        con.query(query6, [0, tipo, ID_Recep, null, null, 1, null], function (error, result, fields) {
            if (error) {
                console.log("dao error CREATE_RECEPCION!!! " + error)
                reject(error);
                return;
            }
            console.log(result);
            resolve(result);
        });
    });
}

const ingresarResultado = function (IDResult, fechaResult, PPM, rut_empleado) {
    console.log("function ( IDResult, rut_empleado ) " + IDResult + " " + rut_empleado);
    let query6 = "SELECT UPDATE_REGISTRO(?,?,?,?,?)";

    return new Promise((resolve, reject) => {
        con.query(query6, [IDResult, fechaResult, PPM, 2, rut_empleado], function (error, result, fields) {
            if (error) {
                console.log("dao error UPDATE_REGISTRO!!! " + error)
                reject(error);
                return;
            }
            console.log(result);
            resolve(result);
        });
    });
}


module.exports.ingresoParticular = ingresoParticular;
module.exports.revisarParticular = revisarParticular;
module.exports.revisarEmpresa = revisarEmpresa;
module.exports.ingresoEmpresa = ingresoEmpresa;
module.exports.getIdEmpresaxRut = getIdEmpresaxRut;
module.exports.ingresoContacto = ingresoContacto;
module.exports.revisarCodigo = revisarCodigo;
module.exports.revisarCodigo2 = revisarCodigo2;
module.exports.ingresarRecepcion = ingresarRecepcion;
module.exports.ingresarRegistro = ingresarRegistro;
module.exports.ingresarResultado = ingresarResultado;